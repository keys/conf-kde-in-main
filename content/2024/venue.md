---
title: Venue
menu:
  "2024":
    weight: 4
---

conf.kde.in is hosted at the COEP Technological University, Pune, Maharashtra.

**Address:** Wellesely Rd, Shivajinagar, Pune, Maharashtra 411005, India 
[**OpenStreetMap**](https://www.openstreetmap.org/?mlat=18.530561&mlon=73.856786&zoom=15#map=15/18.5306/73.8568) - [**Google maps**](https://maps.app.goo.gl/t5c1CgAMdmTauRBq7)

**Website:** <https://www.coep.org.in/>
